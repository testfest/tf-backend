How to start DB:

```shell script
docker run --name testfest_db -d --env POSTGRES_DB=testfest --env POSTGRES_USER=testfest_user --env POSTGRES_PASSWORD=password -p 5432:5432 postgres:alpine
```