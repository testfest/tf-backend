CREATE TABLE IF NOT EXISTS users_courses
(
    user_id   INT REFERENCES users (id) ON DELETE CASCADE UNIQUE,
    course_id INT REFERENCES courses (id) ON DELETE CASCADE
);

INSERT INTO users_courses (user_id, course_id)
VALUES (1, 1);