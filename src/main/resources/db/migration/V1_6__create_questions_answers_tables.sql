CREATE TABLE IF NOT EXISTS questions
(
    id      SERIAL PRIMARY KEY,
    text    VARCHAR NOT NULL,
    points  INTEGER NOT NULL,
    test_id INT REFERENCES tests (id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS answers
(
    id          SERIAL PRIMARY KEY,
    answer      VARCHAR NOT NULL,
    valid       BOOLEAN NOT NULL,
    question_id INT REFERENCES questions (id) ON DELETE CASCADE
);

INSERT INTO questions (text, points, test_id)
VALUES ('Question 11', 50, 1),
       ('Question 12', 50, 1),
       ('Question 21', 40, 2),
       ('Question 22', 40, 2)
ON CONFLICT DO NOTHING;

INSERT INTO answers (answer, valid, question_id)
VALUES ('Answer 11', true, 1),
       ('Answer 12', false, 1),
       ('Answer 13', false, 1),
       ('Answer 21', false, 2),
       ('Answer 22', true, 2),
       ('Answer 23', false, 2),
       ('Answer 24', false, 2),
       ('Answer 31', false, 3),
       ('Answer 32', false, 3),
       ('Answer 33', true, 3),
       ('Answer 41', false, 4),
       ('Answer 42', false, 4),
       ('Answer 43', false, 4),
       ('Answer 44', true, 4),
       ('Answer 45', false, 4)
ON CONFLICT DO NOTHING;