CREATE TABLE IF NOT EXISTS tests
(
    id           SERIAL PRIMARY KEY,
    test_name    VARCHAR NOT NULL,
    points       INTEGER NOT NULL,
    min_points   INTEGER NOT NULL,
    time_minutes INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS tests_courses
(
    test_id   INT REFERENCES tests(id) ON DELETE CASCADE,
    course_id INT REFERENCES courses(id)
);

INSERT INTO tests (test_name, points, min_points, time_minutes)
VALUES ('HTML', 100, 50, 20), ('Java', 120, 70, 40)
ON CONFLICT DO NOTHING;

INSERT INTO tests_courses (test_id, course_id)
VALUES (1, 1), (1, 3), (2, 3)
ON CONFLICT DO NOTHING;