/* username: admin, password: 9McK8FjF6BC4auJp */
INSERT INTO users VALUES (
  0, 'admin', '$2y$10$16ovn5P7szguYN/LNe8ifeZo2.2iS.z5DwfOX4c7i9wRnXJkTRp.O', 'Ivan', 'Ivanovich', 'Ivanov', '1970-01-01'
) ON CONFLICT DO NOTHING;
/* username: hellouser, password: qoQzPBVyTrg3Qdjj */
INSERT INTO users VALUES (
  1, 'hellouser', '$2y$10$22WrZp1rHw/ZQtJqhckqqOxjZin6pXzGHrASUeWT0pcGq2ZA0MLmu', 'Petr', 'Petro', 'Petrov', '1970-02-02'
) ON CONFLICT DO NOTHING;

INSERT INTO roles VALUES (0, 'USER') ON CONFLICT DO NOTHING;
INSERT INTO roles VALUES (1, 'ADMIN') ON CONFLICT DO NOTHING;

INSERT INTO user_roles VALUES (0, 0) ON CONFLICT DO NOTHING;
INSERT INTO user_roles VALUES (0, 1) ON CONFLICT DO NOTHING;
INSERT INTO user_roles VALUES (1, 0) ON CONFLICT DO NOTHING;
