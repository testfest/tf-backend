package it.colbys.testfest.config;

import it.colbys.testfest.core.repository.courserepository.CourseRepository;
import it.colbys.testfest.core.repository.courserepository.DatabaseCourseRepository;
import it.colbys.testfest.core.repository.questionrepository.DatabaseQuestionRepository;
import it.colbys.testfest.core.repository.questionrepository.QuestionRepository;
import it.colbys.testfest.core.repository.testrepository.DatabaseTestRepository;
import it.colbys.testfest.core.repository.testrepository.TestRepository;
import it.colbys.testfest.core.repository.userrepository.DatabaseUserRepository;
import it.colbys.testfest.core.repository.userrepository.UserRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;

@Configuration
public class RepositoryConfig {

    @Bean
    public UserRepository userRepository(@Qualifier("JdbcTemplate") final JdbcTemplate jdbcTemplate) {
        return new DatabaseUserRepository(jdbcTemplate, new GeneratedKeyHolder());
    }

    @Bean
    public CourseRepository courseRepository(@Qualifier("JdbcTemplate") final JdbcTemplate jdbcTemplate) {
        return new DatabaseCourseRepository(jdbcTemplate, new GeneratedKeyHolder());
    }

    @Bean
    public TestRepository testRepository(@Qualifier("JdbcTemplate") final JdbcTemplate jdbcTemplate) {
        return new DatabaseTestRepository(jdbcTemplate, new GeneratedKeyHolder());
    }

    @Bean
    public QuestionRepository questionRepository(@Qualifier("JdbcTemplate") final JdbcTemplate jdbc) {
        return new DatabaseQuestionRepository(jdbc, new GeneratedKeyHolder());
    }
}
