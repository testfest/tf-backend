package it.colbys.testfest.config;

import it.colbys.testfest.core.repository.courserepository.CourseRepository;
import it.colbys.testfest.core.repository.questionrepository.QuestionRepository;
import it.colbys.testfest.core.repository.testrepository.TestRepository;
import it.colbys.testfest.core.repository.userrepository.UserRepository;
import it.colbys.testfest.core.service.certificateservice.CertificateService;
import it.colbys.testfest.core.service.certificateservice.CertificateServiceImpl;
import it.colbys.testfest.core.service.courseservice.CourseService;
import it.colbys.testfest.core.service.courseservice.CourseServiceImpl;
import it.colbys.testfest.core.service.questionservice.QuestionService;
import it.colbys.testfest.core.service.questionservice.QuestionServiceImpl;
import it.colbys.testfest.core.service.testservice.TestService;
import it.colbys.testfest.core.service.testservice.TestServiceImpl;
import it.colbys.testfest.core.service.userservice.UserService;
import it.colbys.testfest.core.service.userservice.UserServiceImpl;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class ServiceConfig {

    @Bean
    @Qualifier("userService")
    public UserService getUserService(
            final UserRepository repository,
            final PasswordEncoder encoder
    ) {
        return new UserServiceImpl(repository, encoder);
    }

    @Bean
    public CourseService getCourseService(
            final CourseRepository courseRepository
    ) {
        return new CourseServiceImpl(courseRepository);
    }

    @Bean
    public TestService getTestService(
            final TestRepository testRepository,
            final CourseRepository courseRepository
    ) {
        return new TestServiceImpl(testRepository, courseRepository);
    }

    @Bean
    public QuestionService getQuestionService(
            final QuestionRepository questionRepository,
            final TestRepository testRepository
    ) {
        return new QuestionServiceImpl(questionRepository, testRepository);
    }

    @Bean
    public CertificateService getCertificateService() {
        return new CertificateServiceImpl();
    }
}
