package it.colbys.testfest.core.service.certificateservice;

import com.itextpdf.io.font.PdfEncodings;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.layout.Canvas;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.TextAlignment;

import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class CertificateServiceImpl implements CertificateService {

    @Override
    public byte[] formatCertificate(String firstName, String lastName, String courseName) {
        byte[] result = new byte[0];
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            URL unicodeFont = Objects.requireNonNull(
                    getClass().getClassLoader().getResource("fonts/NotoSans-Bold.ttf")
            );
            PdfReader reader = new PdfReader("certificate.pdf");
            PdfDocument pdf = new PdfDocument(reader, new PdfWriter(byteArrayOutputStream));
            Document document = new Document(pdf);

            PdfFont font = PdfFontFactory.createFont(unicodeFont.toString(), PdfEncodings.IDENTITY_H);

            PdfCanvas certificateCanvas = new PdfCanvas(pdf.getPage(1));
            Rectangle userRectangle = new Rectangle(10, 343, 400, 50);
            Rectangle courseRectangle = new Rectangle(10, 270, 400, 50);
            Rectangle dateRectangle = new Rectangle(318, 148, 100, 50);

            Canvas userCanvas = new Canvas(certificateCanvas, pdf, userRectangle);
            Canvas courseCanvas = new Canvas(certificateCanvas, pdf, courseRectangle);
            Canvas dateCanvas = new Canvas(certificateCanvas, pdf, dateRectangle);

            userCanvas.add(new Paragraph(String.format("%s %s", firstName, lastName))
                    .setTextAlignment(TextAlignment.CENTER)
                    .setFontSize(24f)
                    .setFont(font));
            courseCanvas.add(new Paragraph(String.format("«%s»", courseName))
                    .setTextAlignment(TextAlignment.CENTER)
                    .setFontSize(22f)
                    .setFont(font));
            dateCanvas.add(new Paragraph(
                    String.format("%s", DateTimeFormatter.ofPattern("dd.MM.yyyy").format(LocalDate.now())
                    ))
                    .setTextAlignment(TextAlignment.LEFT)
                    .setFontSize(16f)
                    .setFont(font));

            document.close();

            result = byteArrayOutputStream.toByteArray();
            byteArrayOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
