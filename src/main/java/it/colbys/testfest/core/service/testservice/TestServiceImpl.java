package it.colbys.testfest.core.service.testservice;

import it.colbys.testfest.core.model.Course;
import it.colbys.testfest.core.model.Test;
import it.colbys.testfest.core.repository.courserepository.CourseRepository;
import it.colbys.testfest.core.repository.testrepository.TestRepository;
import it.colbys.testfest.web.model.response.UserTest;

import java.util.List;
import java.util.stream.Collectors;

public class TestServiceImpl implements TestService {

    private TestRepository repository;
    private CourseRepository courseRepository;

    public TestServiceImpl(final TestRepository repository, final CourseRepository courseRepository) {
        this.repository = repository;
        this.courseRepository = courseRepository;
    }

    @Override
    public List<Test> getAll() {
        return repository.getAll();
    }

    @Override
    public Test getById(String id) {
        return repository.getById(id);
    }

    @Override
    public List<UserTest> getTestsForUser(final String userId, final String courseId) {
        List<Test> tests = repository.getByCourseId(courseId);

        return tests.stream().map(test -> {
            String id = test.getTestId();
            Integer userPoints = repository.getUserPoints(userId, id);

            return new UserTest(
                    id,
                    test.getTestName(),
                    userPoints,
                    test.getPoints(),
                    test.getMinPoints(),
                    test.getQuestionsCount(),
                    test.getTimeMinutes()
            );
        }).collect(Collectors.toList());
    }

    @Override
    public Test addTest(
            final String testName,
            final List<String> coursesIds,
            final Integer points,
            final Integer minPoints,
            final Integer timeMinutes
    ) {
        String id = repository.addTest(testName, coursesIds, points, minPoints, timeMinutes);
        Integer count = 0;
        List<String> courses = coursesIds.stream()
                .map(courseRepository::getById)
                .map(Course::getCourseName)
                .collect(Collectors.toList());
        return new Test(id, testName, courses, points, minPoints, count, timeMinutes);
    }
}
