package it.colbys.testfest.core.service.questionservice;

import it.colbys.testfest.core.model.Question;
import it.colbys.testfest.core.model.TestAnswer;

import java.util.List;

public interface QuestionService {

    List<Question> getByTestId(String testId);

    void calculatePoints(String userId, String testId, List<TestAnswer> testAnswers);

    List<String> addBatch(List<Question> questions, String testId);
}
