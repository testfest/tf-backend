package it.colbys.testfest.core.service.userservice;

import it.colbys.testfest.core.model.User;
import it.colbys.testfest.core.repository.userrepository.UserRepository;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.HashSet;
import java.util.Set;

public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder encoder;

    public UserServiceImpl(
            final UserRepository userRepository,
            final PasswordEncoder encoder
    ) {
        this.userRepository = userRepository;
        this.encoder = encoder;
    }

    @Override
    public User find(String username) {
        return userRepository.getByUsername(username);
    }

    @Override
    public User save(
            final String username,
            final String password,
            final String firstName,
            final String middleName,
            final String lastName,
            final String birthDate
    ) {
        return userRepository.saveUser(
                username,
                encoder.encode(password),
                firstName,
                middleName,
                lastName,
                birthDate
        );
    }

    @Override
    public void delete(String id) {

    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepository.getByUsername(s);
        if (user == null) {
            throw new UsernameNotFoundException("Invalid username or password");
        }
        return new org.springframework.security.core.userdetails.User(
                user.getUsername(),
                user.getPassword(),
                getAuthority(user)
        );
    }

    private Set<SimpleGrantedAuthority> getAuthority(User user) {
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        user.getRoles().forEach(
                role -> authorities.add(new SimpleGrantedAuthority(String.format("ROLE_%s", role.getName())))
        );
        return authorities;
    }
}
