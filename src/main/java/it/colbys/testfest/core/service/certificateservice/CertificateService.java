package it.colbys.testfest.core.service.certificateservice;

public interface CertificateService {

    byte[] formatCertificate(String firstName, String lastName, String courseName);
}
