package it.colbys.testfest.core.service.courseservice;

import it.colbys.testfest.core.model.Course;
import it.colbys.testfest.core.repository.courserepository.CourseRepository;
import it.colbys.testfest.web.model.response.UserTest;

import java.util.ArrayList;
import java.util.List;

public class CourseServiceImpl implements CourseService {

    private CourseRepository repository;

    public CourseServiceImpl(final CourseRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Course> getAll(final String userId) {
        Course course = repository.getByUserId(userId);
        if (course != null) {
            return new ArrayList<Course>() {{
                add(course);
            }};
        } else {
            return repository.getAll();
        }
    }

    @Override
    public boolean validateForCertificate(List<UserTest> tests) {
        boolean result = true;
        for (UserTest test : tests) {
            Integer userPoints = test.getPoints();
            Integer minPoints = test.getMinPoints();
            result &= userPoints >= minPoints;
        }
        return result;
    }

    @Override
    public Course getById(final String id, final String userId) {
        Course userCourse = repository.getByUserId(userId);
        if (userCourse != null) {
            return userCourse;
        }
        Course course = repository.getById(id);
        if (course == null) {
            return null;
        }
        repository.addUserToCourse(userId, id);
        return course;
    }

    @Override
    public Course add(String courseName, String courseDescription) {
        String id = repository.add(courseName, courseDescription);
        return new Course(id, courseName, courseDescription, 0);
    }
}
