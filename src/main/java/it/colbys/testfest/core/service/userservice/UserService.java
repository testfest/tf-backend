package it.colbys.testfest.core.service.userservice;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDataService, UserDetailsService {
}
