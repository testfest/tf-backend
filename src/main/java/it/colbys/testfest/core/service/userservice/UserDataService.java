package it.colbys.testfest.core.service.userservice;

import it.colbys.testfest.core.model.User;

public interface UserDataService {

    User find(String username);

    User save(
            String username,
            String password,
            String firstName,
            String middleName,
            String lastName,
            String birthDate
    );

    void delete(String id);
}
