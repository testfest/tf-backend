package it.colbys.testfest.core.service.testservice;

import it.colbys.testfest.core.model.Test;
import it.colbys.testfest.web.model.response.UserTest;

import java.util.List;

public interface TestService {

    List<Test> getAll();

    Test getById(String id);

    List<UserTest> getTestsForUser(String userId, String courseId);

    Test addTest(
            String testName,
            List<String> coursesIds,
            Integer points,
            Integer minPoints,
            Integer timeMinutes
    );
}
