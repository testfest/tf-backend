package it.colbys.testfest.core.service.questionservice;

import it.colbys.testfest.core.model.Answer;
import it.colbys.testfest.core.model.Question;
import it.colbys.testfest.core.model.TestAnswer;
import it.colbys.testfest.core.repository.questionrepository.QuestionRepository;
import it.colbys.testfest.core.repository.testrepository.TestRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class QuestionServiceImpl implements QuestionService {

    private QuestionRepository repository;
    private TestRepository testRepository;

    public QuestionServiceImpl(
            final QuestionRepository repository,
            final TestRepository testRepository
    ) {
        this.repository = repository;
        this.testRepository = testRepository;
    }

    @Override
    public List<Question> getByTestId(String testId) {
        return repository.getByTestId(testId);
    }

    @Override
    public void calculatePoints(
            final String userId,
            final String testId,
            final List<TestAnswer> testAnswers
    ) {
        List<Question> questions = repository.getByTestId(testId);
        Integer points = 0;

        for (Question question : questions) {
            try {
                String questionId = question.getQuestionId();
                List<Answer> answers = question.getAnswers();
                Stream<TestAnswer> test1 = testAnswers.stream()
                        .filter(testAnswer -> testAnswer.getQuestionId().equals(questionId));
                TestAnswer userAnswer = test1.collect(Collectors.toList()).get(0);
                List<String> userAnswers = userAnswer.getAnswers();

                List<String> validAnswers = answers.stream()
                        .filter(Answer::getValid)
                        .map(Answer::getId)
                        .collect(Collectors.toList());

                if (Arrays.deepEquals(userAnswers.toArray(), validAnswers.toArray())) {
                    points += question.getPoints();
                }
            } catch (IndexOutOfBoundsException ignored) {
            }
        }

        testRepository.saveTestPoints(userId, testId, points);
    }

    @Override
    public List<String> addBatch(List<Question> questions, String testId) {
        List<String> questionIds = new ArrayList<>();
        for (Question question : questions) {
            String questionId = repository.add(question, testId);
            questionIds.add(questionId);
        }
        return questionIds;
    }
}
