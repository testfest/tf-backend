package it.colbys.testfest.core.service.courseservice;

import it.colbys.testfest.core.model.Course;
import it.colbys.testfest.web.model.response.UserTest;

import java.util.List;

public interface CourseService {

    List<Course> getAll(String userId);

    boolean validateForCertificate(List<UserTest> tests);

    Course getById(String id, String userId);

    Course add(String courseName, String courseDescription);
}
