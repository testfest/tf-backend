package it.colbys.testfest.core.repository.questionrepository;

import it.colbys.testfest.core.model.Answer;
import it.colbys.testfest.core.model.Question;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class DatabaseQuestionRepository implements QuestionRepository {

    private JdbcTemplate jdbc;
    private KeyHolder keyHolder;

    public DatabaseQuestionRepository(final JdbcTemplate jdbc, final KeyHolder keyHolder) {
        this.jdbc = jdbc;
        this.keyHolder = keyHolder;
    }

    @Override
    public List<Question> getByTestId(String testId) {
        String questionSql = "SELECT id, text, points FROM questions WHERE test_id = ?";

        List<QuestionDatabase> databaseQuestions = jdbc.query(questionSql, (resultSet, i) -> {
            Integer id = resultSet.getInt("id");
            String text = resultSet.getString("text");
            Integer points = resultSet.getInt("points");
            return new QuestionDatabase(id, text, points);
        }, Integer.parseInt(testId));

        if (databaseQuestions.isEmpty()) {
            return new ArrayList<>();
        }

        String answerSql = "SELECT id, answer, valid FROM answers WHERE question_id = ?";
        return databaseQuestions.stream().map(question -> {
            List<Answer> answers = jdbc.query(answerSql, (resultSet, i) -> {
                String id = String.valueOf(resultSet.getInt("id"));
                String answer = resultSet.getString("answer");
                Boolean valid = resultSet.getBoolean("valid");
                return new Answer(id, answer, valid);
            }, question.id);

            return new Question(String.valueOf(question.id), question.text, question.points, answers);
        }).collect(Collectors.toList());
    }

    @Override
    public String add(final Question question, String testId) {
        String questionSql = "INSERT INTO questions (text, points, test_id) VALUES (?, ?, ?)";

        PreparedStatementCreator insertQuestion = conn -> {
            PreparedStatement ps = conn.prepareStatement(questionSql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, question.getText());
            ps.setInt(2, question.getPoints());
            ps.setInt(3, Integer.parseInt(testId));
            return ps;
        };
        jdbc.update(insertQuestion, keyHolder);

        Integer questionId = (Integer) Objects.requireNonNull(keyHolder.getKeys()).get("id");

        List<Answer> answers = question.getAnswers();
        String answerSql = "INSERT INTO answers (answer, valid, question_id) VALUES (?, ?, ?)";
        jdbc.batchUpdate(answerSql, answers, answers.size(), (ps, answer) -> {
            ps.setString(1, answer.getAnswer());
            ps.setBoolean(2, answer.getValid());
            ps.setInt(3, questionId);
        });

        return String.valueOf(questionId);
    }

    private static class QuestionDatabase {
        public Integer id;
        public String text;
        public Integer points;

        public QuestionDatabase(final Integer id, final String text, final Integer points) {
            this.id = id;
            this.text = text;
            this.points = points;
        }
    }
}
