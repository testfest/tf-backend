package it.colbys.testfest.core.repository.courserepository;

import it.colbys.testfest.core.model.Course;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class DatabaseCourseRepository implements CourseRepository {

    private JdbcTemplate jdbc;
    private KeyHolder keyHolder;

    private RowMapper<CourseDatabase> courseMapper = (resultSet, i) -> {
        Integer id = resultSet.getInt("id");
        String courseName = resultSet.getString("course_name");
        String courseDescription = resultSet.getString("course_description");
        return new CourseDatabase(id, courseName, courseDescription);
    };

    public DatabaseCourseRepository(final JdbcTemplate jdbc, final KeyHolder keyHolder) {
        this.jdbc = jdbc;
        this.keyHolder = keyHolder;
    }

    @Override
    public List<Course> getAll() {
        String sql = "SELECT id, course_name, course_description FROM courses";

        List<CourseDatabase> courses = jdbc.query(sql, courseMapper);

        String countTests = "SELECT count(test_id) from tests_courses WHERE course_id = ?";
        return courses.stream().map(course -> {
            Integer tests = jdbc.queryForObject(countTests, Integer.class, course.id);
            return new Course(String.valueOf(course.id), course.courseName, course.courseDescription, tests);
        }).collect(Collectors.toList());
    }

    @Override
    public Course getByUserId(String userId) {
        String sql = "SELECT c.id, c.course_name, c.course_description " +
                "FROM courses c, users_courses uc WHERE uc.user_id = ? AND uc.course_id = c.id";

        List<CourseDatabase> courses = jdbc.query(sql, courseMapper, Integer.parseInt(userId));

        return getCourse(courses);
    }

    @Override
    public Course getById(String id) {
        String sql = "SELECT id, course_name, course_description FROM courses WHERE id = ?";

        List<CourseDatabase> courses = jdbc.query(sql, courseMapper, Integer.valueOf(id));
        return getCourse(courses);
    }

    private Course getCourse(final List<CourseDatabase> courses) {
        if (courses.isEmpty()) {
            return null;
        }

        String countTests = "SELECT count(test_id) from tests_courses WHERE course_id = ?";
        return courses.stream().map(course -> {
            Integer tests = jdbc.queryForObject(countTests, Integer.class, course.id);
            return new Course(String.valueOf(course.id), course.courseName, course.courseDescription, tests);
        }).collect(Collectors.toList()).get(0);
    }

    @Override
    public String add(String courseName, String courseDescription) {
        String sql = "INSERT INTO courses (course_name, course_description) VALUES (?, ?)";

        PreparedStatementCreator insertCourse = connection -> {
            PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, courseName);
            ps.setString(2, courseDescription);
            return ps;
        };

        jdbc.update(insertCourse, keyHolder);

        Integer id = (Integer) Objects.requireNonNull(keyHolder.getKeys()).get("id");

        return String.valueOf(id);
    }

    @Override
    public void addUserToCourse(String userId, String courseId) {
        String sql = "INSERT INTO users_courses (user_id, course_id) VALUES (?, ?)";

        jdbc.update(sql, Integer.parseInt(userId), Integer.parseInt(courseId));
    }

    private static class CourseDatabase {
        public Integer id;
        public String courseName;
        public String courseDescription;

        public CourseDatabase(final Integer id, final String courseName, final String courseDescription) {
            this.id = id;
            this.courseName = courseName;
            this.courseDescription = courseDescription;
        }
    }
}
