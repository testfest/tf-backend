package it.colbys.testfest.core.repository.userrepository;

import it.colbys.testfest.core.model.Role;
import it.colbys.testfest.core.model.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class DatabaseUserRepository implements UserRepository {

    private JdbcTemplate jdbc;
    private KeyHolder keyHolder;

    public DatabaseUserRepository(
            final JdbcTemplate jdbc,
            final KeyHolder keyHolder
    ) {
        this.jdbc = jdbc;
        this.keyHolder = keyHolder;
    }

    @Override
    public User getByUsername(final String username) {
        String sql = "SELECT\n" +
                "u.id, u.username, u.password, u.first_name, u.middle_name, u.last_name, u.birth_date, " +
                "r.name as role " +
                "FROM users u, roles r, user_roles ur " +
                "WHERE ur.user_id = u.id AND ur.role_id = r.id AND u.username = ?";

        String id = null;
        String password = null;
        String firstName = null;
        String middleName = null;
        String lastName = null;
        String birthDate = null;

        Set<Role> roles = new HashSet<>();

        for (Map<String, Object> row : jdbc.queryForList(sql, username)) {
            String role = String.valueOf(row.get("role"));
            id = String.valueOf(row.get("id"));
            password = String.valueOf(row.get("password"));
            firstName = String.valueOf(row.get("first_name"));
            middleName = String.valueOf(row.get("middle_name"));
            lastName = String.valueOf(row.get("last_name"));
            birthDate = String.valueOf(row.get("birth_date"));
            roles.add(new Role(role));
        }
        if (roles.isEmpty()) {
            return null;
        } else {
            return new User(id, username, password, firstName, middleName, lastName, birthDate, roles);
        }
    }

    @Override
    public User saveUser(
            final String username,
            final String password,
            final String firstName,
            final String middleName,
            final String lastName,
            final String birthDate
    ) {
        String sqlInsertUser = "INSERT " +
                "INTO users (username, password, first_name, middle_name, last_name, birth_date) " +
                "VALUES (?, ?, ?, ?, ?, ?)";
        String sqlGetRoleId = "SELECT id FROM roles WHERE name = 'USER'";
        String sqlInsertRoleRelation = "INSERT INTO user_roles VALUES (?, ?)";

        PreparedStatementCreator insertUser = connection -> {
            PreparedStatement ps = connection.prepareStatement(sqlInsertUser, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, username);
            ps.setString(2, password);
            ps.setString(3, firstName);
            ps.setString(4, middleName);
            ps.setString(5, lastName);
            ps.setString(6, birthDate);
            return ps;
        };

        jdbc.update(insertUser, keyHolder);

        Integer userId = (Integer) Objects.requireNonNull(keyHolder.getKeys()).get("id");
        Integer roleId = jdbc.queryForObject(sqlGetRoleId, (resultSet, i) -> resultSet.getInt("id"));

        jdbc.update(sqlInsertRoleRelation, userId, roleId);

        return new User(
                userId.toString(),
                username,
                password,
                firstName,
                middleName,
                lastName,
                birthDate,
                new HashSet<Role>() {{
                    add(new Role("USER"));
                }});
    }
}
