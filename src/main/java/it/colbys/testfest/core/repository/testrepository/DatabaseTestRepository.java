package it.colbys.testfest.core.repository.testrepository;

import it.colbys.testfest.core.model.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DatabaseTestRepository implements TestRepository {

    private JdbcTemplate jdbc;
    private KeyHolder keyHolder;

    public DatabaseTestRepository(final JdbcTemplate jdbc, final KeyHolder keyHolder) {
        this.jdbc = jdbc;
        this.keyHolder = keyHolder;
    }

    private RowMapper<TestDatabase> rowMapper = (rs, i) -> {
        Integer id = rs.getInt("id");
        String testName = rs.getString("test_name");
        Integer points = rs.getInt("points");
        Integer minPoints = rs.getInt("min_points");
        Integer timeMinutes = rs.getInt("time_minutes");
        return new TestDatabase(id, testName, points, minPoints, timeMinutes);
    };
    private Function<TestDatabase, Test> questionMapper = test -> {
        String coursesSql = "SELECT c.id, c.course_name FROM courses c, tests_courses tc " +
                "WHERE tc.test_id = ? AND c.id = tc.course_id";
        String countSql = "SELECT count(id) FROM questions WHERE test_id = ?";

        List<String> courses = jdbc.query(
                coursesSql, (rs, i) -> rs.getString("course_name"), test.id);
        Integer count = jdbc.queryForObject(countSql, (rs, i) -> rs.getInt("count"), test.id);
        String id = String.valueOf(test.id);

        return new Test(id, test.testName, courses, test.points, test.minPoints, count, test.timeMinutes);
    };

    @Override
    public List<Test> getAll() {
        String sql = "SELECT id, test_name, points, min_points, time_minutes FROM tests";

        List<TestDatabase> databaseTests = jdbc.query(sql, rowMapper);

        return databaseTests.stream().map(questionMapper).collect(Collectors.toList());
    }

    @Override
    public List<Test> getByCourseId(String courseId) {
        String sql = "SELECT t.id, t.test_name, t.points, t.min_points, t.time_minutes FROM tests t " +
                "LEFT JOIN tests_courses tc on t.id = tc.test_id WHERE tc.course_id = ?";

        List<TestDatabase> databaseTests = jdbc.query(sql, rowMapper, Integer.parseInt(courseId));

        return databaseTests.stream().map(questionMapper).collect(Collectors.toList());
    }

    @Override
    public Test getById(String id) {
        String sql = "SELECT id, test_name, points, min_points, time_minutes FROM tests WHERE id = ?";

        List<TestDatabase> databaseTests = jdbc.query(sql, rowMapper, Integer.parseInt(id));

        if (databaseTests.isEmpty()) {
            return null;
        }

        return databaseTests.stream().map(questionMapper).collect(Collectors.toList()).get(0);
    }

    @Override
    public Integer getUserPoints(String userId, String testId) {
        String sql = "SELECT points FROM users_tests WHERE user_id = ? AND test_id = ?";

        List<Integer> points = jdbc.query(
                sql,
                (resultSet, i) -> resultSet.getInt("points"),
                Integer.parseInt(userId),
                Integer.parseInt(testId)
        );
        return points.isEmpty() ? 0 : points.get(0);
    }

    @Override
    public void saveTestPoints(String userId, String testId, Integer points) {
        String sql = "INSERT INTO users_tests (user_id, test_id, points) " +
                "VALUES (?, ?, ?) ON CONFLICT ON CONSTRAINT uqc_user_test " +
                "DO UPDATE SET points = ?";

        Integer userIdInt = Integer.parseInt(userId);
        Integer testIdInt = Integer.parseInt(testId);
        jdbc.update(sql, userIdInt, testIdInt, points, points);
    }


    @Override
    public String addTest(
            final String testName,
            final List<String> coursesIds,
            final Integer points,
            final Integer minPoints,
            final Integer timeMinutes
    ) {
        String sql = "INSERT INTO tests (test_name, points, min_points, time_minutes) VALUES (?, ?, ?, ?)";

        PreparedStatementCreator insertTest = connection -> {
            PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, testName);
            ps.setInt(2, points);
            ps.setInt(3, minPoints);
            ps.setInt(4, timeMinutes);
            return ps;
        };
        jdbc.update(insertTest, keyHolder);

        Integer testId = (Integer) Objects.requireNonNull(keyHolder.getKeys()).get("id");

        String courseSql = "INSERT INTO tests_courses (test_id, course_id) VALUES (?, ?)";
        jdbc.batchUpdate(courseSql, coursesIds, coursesIds.size(), (ps, s) -> {
            ps.setInt(1, testId);
            ps.setInt(2, Integer.parseInt(s));
        });

        return String.valueOf(testId);
    }

    private static class TestDatabase {

        public Integer id;
        public String testName;
        public Integer points;
        public Integer minPoints;
        public Integer timeMinutes;

        public TestDatabase(
                final Integer id,
                final String testName,
                final Integer points,
                final Integer minPoints,
                final Integer timeMinutes
        ) {
            this.id = id;
            this.testName = testName;
            this.points = points;
            this.minPoints = minPoints;
            this.timeMinutes = timeMinutes;
        }
    }
}
