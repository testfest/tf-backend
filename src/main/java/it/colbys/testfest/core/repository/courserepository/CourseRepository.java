package it.colbys.testfest.core.repository.courserepository;

import it.colbys.testfest.core.model.Course;

import java.util.List;

public interface CourseRepository {

    List<Course> getAll();

    Course getByUserId(String userId);

    Course getById(String id);

    String add(String courseName, String courseDescription);

    void addUserToCourse(String userId, String courseId);
}
