package it.colbys.testfest.core.repository.questionrepository;

import it.colbys.testfest.core.model.Question;

import java.util.List;

public interface QuestionRepository {

    List<Question> getByTestId(String testId);

    String add(Question question, String testId);
}
