package it.colbys.testfest.core.repository.testrepository;

import it.colbys.testfest.core.model.Test;

import java.util.List;

public interface TestRepository {

    List<Test> getAll();

    List<Test> getByCourseId(String courseId);

    Test getById(String id);

    Integer getUserPoints(String userId, String testId);

    void saveTestPoints(String userId, String testId, Integer points);

    String addTest(
            String testName,
            List<String> coursesIds,
            Integer points,
            Integer minPoints,
            Integer timeMinutes
    );
}
