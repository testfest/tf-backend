package it.colbys.testfest.core.repository.userrepository;

import it.colbys.testfest.core.model.User;

public interface UserRepository {

    User getByUsername(String username);

    User saveUser(
            String username,
            String password,
            String firstName,
            String middleName,
            String lastName,
            String birthDate
    );
}
