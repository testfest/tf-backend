package it.colbys.testfest.core.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Answer {

    private String id;
    private String answer;
    @JsonIgnore
    private Boolean valid;

    public Answer(final String answer, final Boolean valid) {
        this.id = "";
        this.answer = answer;
        this.valid = valid;
    }

    public Answer(final String id, final String answer, final Boolean valid) {
        this.id = id;
        this.answer = answer;
        this.valid = valid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }
}
