package it.colbys.testfest.core.model;

public class Course {

    private String id;
    private String courseName;
    private String courseDescription;
    private Integer testsCount;

    public Course(
            final String id,
            final String courseName,
            final String courseDescription,
            final Integer testsCount
    ) {
        this.id = id;
        this.courseName = courseName;
        this.courseDescription = courseDescription;
        this.testsCount = testsCount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseDescription() {
        return courseDescription;
    }

    public void setCourseDescription(String courseDescription) {
        this.courseDescription = courseDescription;
    }

    public Integer getTestsCount() {
        return testsCount;
    }

    public void setTestsCount(Integer testsCount) {
        this.testsCount = testsCount;
    }
}
