package it.colbys.testfest.core.model;

import java.util.List;

public class TestAnswer {

    private String questionId;
    private List<String> answers;

    public TestAnswer(final String questionId, final List<String> answers) {
        this.questionId = questionId;
        this.answers = answers;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public List<String> getAnswers() {
        return answers;
    }

    public void setAnswers(List<String> answers) {
        this.answers = answers;
    }
}
