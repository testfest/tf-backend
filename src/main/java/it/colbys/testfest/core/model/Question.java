package it.colbys.testfest.core.model;

import java.util.List;

public class Question {

    private String questionId;
    private String text;
    private Integer points;
    private List<Answer> answers;

    public Question(final String text, final Integer points, final List<Answer> answers) {
        this.questionId = "";
        this.text = text;
        this.points = points;
        this.answers = answers;
    }

    public Question(
            final String questionId,
            final String text,
            final Integer points,
            final List<Answer> answers
    ) {
        this.questionId = questionId;
        this.text = text;
        this.points = points;
        this.answers = answers;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }
}
