package it.colbys.testfest.core.model;

import java.util.List;

public class Test {

    private String testId;
    private String testName;
    private List<String> courses;
    private Integer points;
    private Integer minPoints;
    private Integer questionsCount;
    private Integer timeMinutes;

    public Test(
            final String testId,
            final String testName,
            final List<String> courses,
            final Integer points,
            final Integer minPoints,
            final Integer questionsCount,
            final Integer timeMinutes
    ) {
        this.testId = testId;
        this.testName = testName;
        this.courses = courses;
        this.points = points;
        this.minPoints = minPoints;
        this.questionsCount = questionsCount;
        this.timeMinutes = timeMinutes;
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public List<String> getCourses() {
        return courses;
    }

    public void setCourses(List<String> courses) {
        this.courses = courses;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Integer getMinPoints() {
        return minPoints;
    }

    public void setMinPoints(Integer minPoints) {
        this.minPoints = minPoints;
    }

    public Integer getQuestionsCount() {
        return questionsCount;
    }

    public void setQuestionsCount(Integer questionsCount) {
        this.questionsCount = questionsCount;
    }

    public Integer getTimeMinutes() {
        return timeMinutes;
    }

    public void setTimeMinutes(Integer timeMinutes) {
        this.timeMinutes = timeMinutes;
    }
}
