package it.colbys.testfest.core.model;

public class Constants {

    public static final long ACCESS_TOKEN_VALIDITY_SECONDS = 5 * 60 * 60;
    public static final String SIGNING_KEY = "colbys@testfest.ru";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";

    public static final String AUTHORITIES_KEY = "roles";
    public static final String FIRST_NAME_KEY = "fname";
    public static final String LAST_NAME_KEY = "lname";
    public static final String USER_ID_KEY = "uid";
}
