package it.colbys.testfest.core.model;

public class Role {

    private String name;

    public Role() {}

    public Role(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
