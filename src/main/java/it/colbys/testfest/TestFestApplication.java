package it.colbys.testfest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestFestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestFestApplication.class, args);
	}

}
