package it.colbys.testfest.web.model.request;

import java.util.List;

public class CreateTestRequest {

    private String testName;
    private List<String> coursesIds;
    private Integer points;
    private Integer minPoints;
    private Integer timeMinutes;

    public CreateTestRequest(
            final String testName,
            final List<String> coursesIds,
            final Integer points,
            final Integer minPoints,
            final Integer timeMinutes
    ) {
        this.testName = testName;
        this.coursesIds = coursesIds;
        this.points = points;
        this.minPoints = minPoints;
        this.timeMinutes = timeMinutes;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public List<String> getCoursesIds() {
        return coursesIds;
    }

    public void setCoursesIds(List<String> coursesIds) {
        this.coursesIds = coursesIds;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Integer getMinPoints() {
        return minPoints;
    }

    public void setMinPoints(Integer minPoints) {
        this.minPoints = minPoints;
    }

    public Integer getTimeMinutes() {
        return timeMinutes;
    }

    public void setTimeMinutes(Integer timeMinutes) {
        this.timeMinutes = timeMinutes;
    }
}
