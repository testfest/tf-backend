package it.colbys.testfest.web.model.request;

import it.colbys.testfest.core.model.TestAnswer;

import java.util.List;

public class SubmitResultsRequest {

    private String testId;
    private List<TestAnswer> results;

    public SubmitResultsRequest(final String testId, final List<TestAnswer> results) {
        this.testId = testId;
        this.results = results;
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public List<TestAnswer> getResults() {
        return results;
    }

    public void setResults(List<TestAnswer> results) {
        this.results = results;
    }
}
