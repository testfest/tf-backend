package it.colbys.testfest.web.model.request;

import java.util.List;

public class CreateQuestionsRequest {

    private String testId;
    private List<Question> questions;

    public CreateQuestionsRequest(final String testId, final List<Question> questions) {
        this.testId = testId;
        this.questions = questions;
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public static class Question {
        private String text;
        private Integer points;
        private List<Answer> answers;

        public Question(final String text, final Integer points, final List<Answer> answers) {
            this.text = text;
            this.points = points;
            this.answers = answers;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public Integer getPoints() {
            return points;
        }

        public void setPoints(Integer points) {
            this.points = points;
        }

        public List<Answer> getAnswers() {
            return answers;
        }

        public void setAnswers(List<Answer> answers) {
            this.answers = answers;
        }
    }

    public static class Answer {
        private String answer;
        private Boolean valid;

        public Answer(final String answer, final Boolean valid) {
            this.answer = answer;
            this.valid = valid;
        }

        public String getAnswer() {
            return answer;
        }

        public void setAnswer(String answer) {
            this.answer = answer;
        }

        public Boolean getValid() {
            return valid;
        }

        public void setValid(Boolean valid) {
            this.valid = valid;
        }
    }
}
