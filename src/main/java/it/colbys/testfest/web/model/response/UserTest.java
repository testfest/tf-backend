package it.colbys.testfest.web.model.response;

public class UserTest {

    private String testId;
    private String testName;
    private Integer points;
    private Integer maxPoints;
    private Integer minPoints;
    private Integer questionsCount;
    private Integer timeMinutes;

    public UserTest(
            final String testId,
            final String testName,
            final Integer points,
            final Integer maxPoints,
            final Integer minPoints,
            final Integer questionsCount,
            final Integer timeMinutes
    ) {
        this.testId = testId;
        this.testName = testName;
        this.points = points;
        this.maxPoints = maxPoints;
        this.minPoints = minPoints;
        this.questionsCount = questionsCount;
        this.timeMinutes = timeMinutes;
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Integer getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(Integer maxPoints) {
        this.maxPoints = maxPoints;
    }

    public Integer getMinPoints() {
        return minPoints;
    }

    public void setMinPoints(Integer minPoints) {
        this.minPoints = minPoints;
    }

    public Integer getQuestionsCount() {
        return questionsCount;
    }

    public void setQuestionsCount(Integer questionsCount) {
        this.questionsCount = questionsCount;
    }

    public Integer getTimeMinutes() {
        return timeMinutes;
    }

    public void setTimeMinutes(Integer timeMinutes) {
        this.timeMinutes = timeMinutes;
    }
}
