package it.colbys.testfest.web.controller;

import it.colbys.testfest.core.model.Answer;
import it.colbys.testfest.core.model.Constants;
import it.colbys.testfest.core.model.Question;
import it.colbys.testfest.core.security.TokenProvider;
import it.colbys.testfest.core.service.questionservice.QuestionService;
import it.colbys.testfest.web.model.request.CreateQuestionsRequest;
import it.colbys.testfest.web.model.request.SubmitResultsRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/questions")
public class QuestionController {

    private QuestionService service;
    private TokenProvider tokenProvider;

    public QuestionController(
            final QuestionService service,
            final TokenProvider tokenProvider) {
        this.service = service;
        this.tokenProvider = tokenProvider;
    }

    @GetMapping
    @ResponseBody
    public ResponseEntity<List<Question>> getQuestionsForTest(@RequestParam("testId") final String testId) {
        return ResponseEntity.status(HttpStatus.OK).body(service.getByTestId(testId));
    }

    @PostMapping("/results")
    @ResponseBody
    public void submitResults(
            @RequestHeader("Authorization") final String bearer,
            @RequestBody final SubmitResultsRequest request
    ) {
        String userId = tokenProvider.getClaimFromToken(
                bearer.replace(Constants.TOKEN_PREFIX, ""), Constants.USER_ID_KEY);
        service.calculatePoints(userId, request.getTestId(), request.getResults());
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping
    @ResponseBody
    public ResponseEntity<List<String>> addQuestions(@RequestBody final CreateQuestionsRequest request) {
        List<Question> questions = request.getQuestions().stream().map(question -> {
            String text = question.getText();
            Integer points = question.getPoints();
            List<Answer> answers = question.getAnswers()
                    .stream().map(answer -> new Answer(answer.getAnswer(), answer.getValid()))
                    .collect(Collectors.toList());

            return new Question(text, points, answers);
        }).collect(Collectors.toList());

        List<String> ids = service.addBatch(questions, request.getTestId());
        return ResponseEntity.status(HttpStatus.CREATED).body(ids);
    }
}
