package it.colbys.testfest.web.controller;

import it.colbys.testfest.core.model.Constants;
import it.colbys.testfest.core.model.Test;
import it.colbys.testfest.core.security.TokenProvider;
import it.colbys.testfest.core.service.testservice.TestService;
import it.colbys.testfest.web.model.request.CreateTestRequest;
import it.colbys.testfest.web.model.response.UserTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/tests")
public class TestController {

    private TestService service;
    private TokenProvider tokenProvider;

    public TestController(final TestService service, final TokenProvider tokenProvider) {
        this.service = service;
        this.tokenProvider = tokenProvider;
    }

    @GetMapping
    @ResponseBody
    public ResponseEntity<List<UserTest>> getTestsForUser(
            @RequestParam("courseId") final String courseId,
            @RequestHeader("Authorization") final String bearer
    ) {
        String userId = tokenProvider.getClaimFromToken(
                bearer.replace(Constants.TOKEN_PREFIX, ""), Constants.USER_ID_KEY);
        List<UserTest> tests = service.getTestsForUser(userId, courseId);
        return ResponseEntity.status(HttpStatus.OK).body(tests);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/all")
    @ResponseBody
    public ResponseEntity<List<Test>> getTests() {
        return ResponseEntity.status(HttpStatus.OK).body(service.getAll());
    }

    @GetMapping("/{id}")
    @ResponseBody
    public ResponseEntity<Test> getTestById(@PathVariable final String id) {
        Test test = service.getById(id);
        if (test == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.status(HttpStatus.OK).body(test);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping
    @ResponseBody
    public ResponseEntity<Test> createTest(@RequestBody final CreateTestRequest request) {
        String testName = request.getTestName();
        List<String> coursesIds = request.getCoursesIds();
        Integer points = request.getPoints();
        Integer minPoints = request.getMinPoints();
        Integer timeMinutes = request.getTimeMinutes();

        Test test = service.addTest(testName, coursesIds, points, minPoints, timeMinutes);

        return ResponseEntity.status(HttpStatus.CREATED).body(test);
    }
}
