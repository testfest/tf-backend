package it.colbys.testfest.web.controller;

import it.colbys.testfest.core.model.AuthToken;
import it.colbys.testfest.core.model.User;
import it.colbys.testfest.core.service.userservice.UserDataService;
import it.colbys.testfest.web.model.request.SignUpUser;
import it.colbys.testfest.web.model.request.SignInUser;
import it.colbys.testfest.core.security.TokenProvider;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    private UserDataService userService;
    private AuthenticationManager authenticationManager;
    private TokenProvider jwtTokenUtil;

    public UserController(
            @Qualifier("userService") final UserDataService userService,
            final AuthenticationManager authenticationManager,
            final TokenProvider jwtTokenUtil
    ) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
    }

    @PostMapping(value = "/sign-in")
    public ResponseEntity<?> register(@RequestBody final SignInUser signInUser) throws AuthenticationException {
        Authentication auth = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        signInUser.getUsername(),
                        signInUser.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(auth);
        User user = userService.find(signInUser.getUsername());
        String token = jwtTokenUtil.generateToken(auth, user);
        return ResponseEntity.ok(new AuthToken(token));
    }

    @PostMapping(value = "/sign-up")
    public ResponseEntity<?> signUp(@RequestBody final SignUpUser signUpUser) {
        User user = userService.save(
                signUpUser.getUsername(),
                signUpUser.getPassword(),
                signUpUser.getFirstName(),
                signUpUser.getMiddleName(),
                signUpUser.getLastName(),
                signUpUser.getBirthDate()
        );

        Authentication auth = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        signUpUser.getUsername(),
                        signUpUser.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(auth);
        String token = jwtTokenUtil.generateToken(auth, user);
        return ResponseEntity.status(HttpStatus.CREATED).body(new AuthToken(token));
    }
}
