package it.colbys.testfest.web.controller;

import it.colbys.testfest.core.model.Constants;
import it.colbys.testfest.core.model.Course;
import it.colbys.testfest.core.security.TokenProvider;
import it.colbys.testfest.core.service.certificateservice.CertificateService;
import it.colbys.testfest.core.service.courseservice.CourseService;
import it.colbys.testfest.core.service.testservice.TestService;
import it.colbys.testfest.web.model.request.CreateCourseRequest;
import it.colbys.testfest.web.model.response.UserTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/courses")
public class CourseController {

    private CourseService courseService;
    private TestService testService;
    private CertificateService certificateService;
    private TokenProvider tokenProvider;

    public CourseController(
            final CourseService courseService,
            final TokenProvider tokenProvider,
            final TestService testService,
            final CertificateService certificateService) {
        this.courseService = courseService;
        this.tokenProvider = tokenProvider;
        this.testService = testService;
        this.certificateService = certificateService;
    }

    @GetMapping
    @ResponseBody
    public ResponseEntity<List<Course>> getCourses(@RequestHeader("Authorization") final String bearer) {
        String token = bearer.replace(Constants.TOKEN_PREFIX, "");
        String userId = tokenProvider.getClaimFromToken(token, Constants.USER_ID_KEY);
        List<Course> courses = courseService.getAll(userId);
        if (courses.size() == 1) {
            return ResponseEntity.status(HttpStatus.MULTI_STATUS).body(courses);
        }
        return ResponseEntity.status(HttpStatus.OK).body(courses);
    }

    @GetMapping(value = "/{id}/certificate")
    @ResponseBody
    public ResponseEntity<byte[]> getCertificate(
            @PathVariable String id,
            @RequestHeader("Authorization") final String bearer
    ) {
        String token = bearer.replace(Constants.TOKEN_PREFIX, "");
        String userId = tokenProvider.getClaimFromToken(token, Constants.USER_ID_KEY);
        String firstName = tokenProvider.getClaimFromToken(token, Constants.FIRST_NAME_KEY);
        String lastName = tokenProvider.getClaimFromToken(token, Constants.LAST_NAME_KEY);

        List<UserTest> tests = testService.getTestsForUser(userId, id);
        if (!courseService.validateForCertificate(tests)) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }

        Course course = courseService.getById(id, userId);

        byte[] certificate = certificateService.formatCertificate(firstName, lastName, course.getCourseName());

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_PDF)
                .body(certificate);
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<Course> getCourse(
            @PathVariable final String id,
            @RequestHeader("Authorization") final String bearer
    ) {
        String userId = tokenProvider.getClaimFromToken(
                bearer.replace(Constants.TOKEN_PREFIX, ""), Constants.USER_ID_KEY);
        Course course = courseService.getById(id, userId);
        if (course == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.status(HttpStatus.OK).body(course);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping
    @ResponseBody
    public ResponseEntity<Course> createCourse(@RequestBody final CreateCourseRequest request) {
        String courseName = request.getCourseName();
        String courseDescription = request.getCourseDescription();

        Course course = courseService.add(courseName, courseDescription);
        return ResponseEntity.status(HttpStatus.CREATED).body(course);
    }
}
